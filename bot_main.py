#!/usr/bin/python3

import logging
from aiogram import Bot, Dispatcher, executor, types
from nosql.query import universal_query
from nosql import mongo_set
import time
from telegram import api_token
from archivador import archivero
import utm
from nosql.registro_consulta import Registrar

API_TOKEN = api_token.API_TOKEN
# Configure logging
logging.basicConfig(level=logging.INFO)
# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


def mongo_setup():
    mongo_set.global_init()


class Chateo:
    """
    en caso de necesitar una identificacion o envio de informacion dejo esto por aqui para luego acceder a esta funcion
    """

    def __init__(self, diccionario):
        for key in diccionario:
            if key == 'from':
                setattr(self, 'desde', diccionario[key])
            else:
                setattr(self, key, diccionario[key])


@dp.message_handler(commands=['start', 'ayuda'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` or `/help` command
    """
    aqui = Registrar(nombretelegram=message.from_user.full_name,
                     idtelegram=message.from_user.id,
                     consulta='Uso comando /start')
    aqui.organiza()
    await message.reply("Hola \n"
                        "Soy 🤖 ElBotDeConsultaCatastral 🤖\n"
                        "Conmigo puedes consultar las Parcelas Resultantes De La Cartografia"
                        "puedes consultar directamente introduciendo:\n\n"
                        "1- Posicional:Ejemplo < '308362848158' >\n"
                        "Sin comandos directamente en el chat.\n"
                        "🛰️\n"
                        "\n2- No Expediete: comenzando por 663\n"
                        "  -Ejemplo: 663201504000 devuelve posicional\n"
                        "\n3- Enviar Ubicacion: puedes enviar tu \n"
                        "ubicación y te devolveré un mosaico de \n"
                        "al menos 10 resultantes y las Historicas\n"
                        "cercanas para poder consultar de forma\n"
                        "individual clickando 📎 aqui debajo 👇")


excell = None
kml = None
csv = None
dxf = None
posicional = None
chater = None
informe = None
archivo_nombre = None
historicados = None
identificador = None


@dp.message_handler(content_types=types.ContentType.LOCATION)
async def busca_por_coordenadas(message: types.Location):
    global historicados
    # siempre recuerda pedir a los mensajes al programar to_python porque algunas
    latitud = message.location.latitude
    # funciones no reconocen todos los aatributos de los mensajes
    longitud = message.location.longitude
    texto = f'El usuario {message.from_user.id}:{message.from_user.full_name} esta' \
            f'buscando parcelas cercanas a ubicacion {latitud}, {longitud}'
    aqui = Registrar(nombretelegram=message.from_user.full_name,
                     idtelegram=message.from_user.id,
                     consulta=texto)
    aqui.organiza()
    resultados = [str(x.posicional) for x in universal_query(
        4, lat=latitud, long=longitud, cantidad=10)]
    resultados1 = [str(x.parcela) for x in universal_query(
        5, lat=latitud, long=longitud, cantidad=4)]
    historicados = universal_query(5, lat=latitud, long=longitud, cantidad=4)

    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
    keyboard_markup1 = types.InlineKeyboardMarkup(row_width=3)
    conteo = len(resultados)
    conteo1 = len(resultados1)
    boton = 0
    await message.answer(f"🛰️ Ubicacion: ({latitud}, {longitud}) 🛰️")
    if resultados:
        for x in range(conteo):
            boton += 2
            row_btn = (types.InlineKeyboardButton(text, callback_data=text)
                       for text in resultados[boton - 2:boton])
            keyboard_markup.row(*row_btn)
            if boton >= conteo:
                break
        await message.answer(f"Parcelas resultantes Cercanas",
                             reply_markup=keyboard_markup)

    else:
        await message.answer(f"No Encontre Nada cercano")

    boton = 0
    if resultados1:
        for x in range(conteo1):
            boton += 2
            row_btn = (types.InlineKeyboardButton(text, callback_data=text)
                       for text in resultados1[boton - 2:boton])
            keyboard_markup1.row(*row_btn)
            if boton >= conteo1:
                break
        await message.answer(f"Parcelas Historicas Cercanas",
                             reply_markup=keyboard_markup1)
    else:
        await message.answer(f"No Encontre Nada cercano")


"""aqui esta el final de los historicos esto dara la localizaciion de los historicos"""


@dp.message_handler(regexp=('[3,4]\d{11}'))
async def send_posicional(message: types.Message):
    """
    This handler will be called when user consulta `/consulta` o `/hola` command
    """
    global csv, posicional, chater, identificador
    valores = message.to_python()
    if identificador == None:
        identificador = Chateo(valores)
    # print(identificador.desde.get('id')) para verificar id del que envia
    chater = message.chat.id
    keyboard_markup = types.InlineKeyboardMarkup(
        row_width=4, inline_keyboard=True)
    b = message.text
    esto = universal_query(1, num=b)
    texto = f'el usuario {message.from_user.id}:{message.from_user.full_name} esta buscando posicional {b}'
    aqui = Registrar(nombretelegram=message.from_user.full_name,
                     idtelegram=message.from_user.id,
                     consulta=texto)
    aqui.organiza()
    if esto:
        await message.reply(f'Posicional encontrado')
        time.sleep(1)
        row_btn = (types.InlineKeyboardButton(text, callback_data=text)
                   for text in [esto.posicional])
        keyboard_markup.row(*row_btn)
        await message.answer("Selecciona el Posicional a Consultar",
                             reply_markup=keyboard_markup)
    else:
        await message.reply(f'Posicional no encontrado')


##############################################################################
@dp.message_handler(regexp=('66\d{10}'))
async def send_expediente(message: types.Message):
    """
    This handler will be called when user consulta `/consulta` o `/hola` command
    """
    global csv, posicional, chater
    chater = message.chat.id
    keyboard_markup = types.InlineKeyboardMarkup(
        row_width=4, inline_keyboard=True)
    b = message.text.replace('-', '')
    esto = universal_query(2, num=b)
    texto = f'El Usuario {message.from_user.id}:{message.from_user.full_name} Esta buscando expediente {b}'
    aqui = Registrar(nombretelegram=message.from_user.full_name,
                     idtelegram=message.from_user.id,
                     consulta=texto)
    aqui.organiza()
    if esto:
        await message.reply(f'Expediente encontrado')
        time.sleep(1)
        row_btn = (types.InlineKeyboardButton(text, callback_data=text)
                   for text in [esto.posicional])
        keyboard_markup.row(*row_btn)
        await message.answer("Selecciona el Posicional a Consultar",
                             reply_markup=keyboard_markup)
    else:
        await message.reply(f'Expediente no encontrado')


@dp.message_handler(regexp='.(\.csv)')
@dp.message_handler(content_types=types.ContentType.DOCUMENT)
async def convert(message: types.Message):
    """Ejemplo de el diccionario retornado de la funcion .to_python() del objetto retornado del message_handler
    {'message_id': 1623, 'from': {'id': 231841856, 'is_bot': False, 'first_name': 'Xilejezo', 'language_code': 'en'},
     'chat': {'id': 231841856, 'first_name': 'Xilejezo', 'type': 'private'},
     'date': 1578280314,
     'document': {'file_name': '1_4942741556437188709.csv', 'mime_type': 'text/comma-separated-values',
                  'file_id': 'BQADAQADZgADQiWYRLrg9ocFn97iFgQ', 'file_unique_id': 'AgADZgADQiWYRA', 'file_size': 331}}
"""
    global archivo_nombre
    archivo_dict = message.to_python()
    desde = archivo_dict['from']['id']
    archivo_nombre = archivo_dict['document']['file_name']
    archivo_tipo = archivo_dict['document']['mime_type']
    es_csv = archivo_tipo == 'text/comma-separated-values'
    # print(f'archivo enviado {message.to_python()}')
    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
    botones = (('A KML', 'A KML'),
               ('A DXF', 'A DXF'),
               ('A Excell', 'A Excell'))
    row_btn = (types.InlineKeyboardButton(text, callback_data=text)
               for text, data in botones)
    keyboard_markup.row(*row_btn)
    await message.bot.send_message(
        chat_id=desde,
        text=f'Haz enviado archivo {archivo_nombre} csv? {es_csv}'
        f' desde {desde} que deseas Hacer?...')
    await message.bot.send_message(chat_id=desde,
                                   reply_markup=keyboard_markup,
                                   text='Tenemos las '
                                   'siguientes converciones...')


@dp.callback_query_handler(text='A KML')
@dp.callback_query_handler(text='A Excell')
@dp.callback_query_handler(text='A DXF')
@dp.callback_query_handler(text='Archivo CSV')
@dp.callback_query_handler(text='Ubicacion')
@dp.callback_query_handler(text='Cercanas')
async def inline_answer_callbafileck_handler(query: types.CallbackQuery):
    global csv, excell, kml, dxf
    answer_data = query.data
    if answer_data == 'Archivo CSV':
        texto = f'el usuario {query.from_user.id}:{query.from_user.full_name} esta buscando Csv {posicional}'
        aqui = Registrar(nombretelegram=query.from_user.full_name,
                         idtelegram=query.from_user.id,
                         consulta=texto)
        aqui.organiza()
        # print(texto)
        archivero.verifica_archivo(posicional,
                                   informe.coordenadas['coordinates'])
        with open(csv)as file:
            await query.message.reply_document(
                file,
                caption='Este es el archivo CSV!!!')

    elif answer_data == 'Ubicacion':
        texto = f'el usuario {query.from_user.id}:{query.from_user.full_name} esta buscando ubicacion {posicional}'
        aqui = Registrar(nombretelegram=query.from_user.full_name,
                         idtelegram=query.from_user.id,
                         consulta=texto)
        aqui.organiza()
        text = [str(posicional)[::2], str(posicional)[1::2]]
        x = int(text[0])
        y = int('2' + text[1])
        # await bot.send_message(query.from_user.id, text)
        lat, long = utm.to_latlon(x, y, 19, 'q')
        # print(lat, long)
        await bot.send_location(query.from_user.id, lat, long)
    elif answer_data == 'Cercanas':
        texto = f'el usuario {query.from_user.id}:{query.from_user.full_name} esta buscando parcelas cercanas a {posicional}'
        aqui = Registrar(nombretelegram=query.from_user.full_name,
                         idtelegram=query.from_user.id,
                         consulta=texto)
        aqui.organiza()
        text = [str(posicional)[::2], str(posicional)[1::2]]
        x = int(text[0])
        y = int('2' + text[1])
        lat, long = utm.to_latlon(x, y, 19, 'q')
        resultados = [str(x.posicional)
                      for x in universal_query(4,
                                               lat=lat,
                                               long=long,
                                               cantidad=10)]
        keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
        conteo = len(resultados)
        boton = 0
        for x in range(conteo):
            boton += 2
            row_btn = (types.InlineKeyboardButton(text, callback_data=text)
                       for text in resultados[boton - 2:boton])
            keyboard_markup.row(*row_btn)
            if boton >= conteo:
                break
        await query.message.answer(
            f"Parcelas Cercanas a Ubicacion {posicional}",
            reply_markup=keyboard_markup)

    elif answer_data == 'A KML':
        texto = f'el usuario {query.from_user.id}:{query.from_user.full_name} esta buscando KML {posicional}'
        aqui = Registrar(nombretelegram=query.from_user.full_name,
                         idtelegram=query.from_user.id,
                         consulta=texto)
        aqui.organiza()
        archivero.crea_kml(posicional, informe.coordenadas['coordinates'])
        with open(kml, 'rb')as file:
            await query.message.reply_document(file, caption='Este es su KML!')
    elif answer_data == 'A DXF':
        texto = f'el usuario {query.from_user.id}:{query.from_user.full_name} esta buscando DXF {posicional}'
        aqui = Registrar(nombretelegram=query.from_user.full_name,
                         idtelegram=query.from_user.id,
                         consulta=texto)
        aqui.organiza()
        archivero.crea_dxf(posicional, informe.coordenadas['coordinates'])
        with open(dxf, 'rb')as file:
            await query.message.reply_document(file, caption='Este es su DXF!')

    elif answer_data == 'A Excell':
        texto = f'el usuario {query.from_user.id}:{query.from_user.full_name} esta buscando Excell {posicional}'
        aqui = Registrar(
            nombretelegram=query.from_user.full_name,
            idtelegram=query.from_user.id,
            consulta=texto)
        aqui.organiza()
        archivero.crea_xls(posicional, informe.coordenadas['coordinates'])
        with open(excell, 'rb')as file:
            await query.message.reply_document(file, caption='Este es su Excell!')
    else:
        text = f'Unexpected callback data {answer_data!r}!'
    await bot.send_message(query.from_user.id, 'Usa /ayuda para ver más opciones')


@dp.callback_query_handler(regexp='\d{12}')
async def inline_kb_answer_callback_handler(query: types.CallbackQuery):
    global posicional, csv, informe, excell, kml, dxf
    answer_data = query.data
    if answer_data.isdigit():
        posicional = answer_data
        keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
        data_btn = (('Archivo CSV', 'Archivo CSV'),
                    ('Ubicacion', 'Ubicacion'),
                    ('Cercanas', 'Cercanas'))
        data_btn1 = (('A KML', 'A KML'),
                     ('A DXF', 'A DXF'),
                     ('A Excell', 'A Excell'))
        informe = universal_query(1, num=posicional)
        expediente = informe.expediente
        temporal = informe.parcelatemporal
        fechacreacion = informe.fechaCreaccion
        area = informe.area
        texto = (
            f'el usuario {query.from_user.id}:{query.from_user.full_name} esta buscando {posicional}')
        aqui = Registrar(nombretelegram=query.from_user.full_name,
                         idtelegram=query.from_user.id, consulta=texto)
        aqui.organiza()
        row_btn = (types.InlineKeyboardButton(
            text, callback_data=text) for text, data in data_btn)
        keyboard_markup.row(*row_btn)
        row_btn1 = (types.InlineKeyboardButton(
            text, callback_data=text) for text, data in data_btn1)
        keyboard_markup.row(*row_btn1)
        await query.message.reply(
            f"El Posicional {posicional}\nExpediente {expediente}\n"
            f"Fecha de Creacion {fechacreacion}\narea {area} m2")

        csv = ("./archivos/csv/" + posicional + ".csv")
        excell = ("./archivos/excell/" + posicional + ".xlsx")
        kml = ("./archivos/kml/" + posicional + ".kml")
        dxf = ("./archivos/dxf/" + posicional + ".dxf")
        await query.message.answer("Que Deseas Hacer?",
                                   reply_markup=keyboard_markup)


##############################################################################
@dp.callback_query_handler(regexp='[\d]{1,7}-?\d?\w?')
async def inline_kb_answer_callback_handler(query: types.CallbackQuery):
    global posicional, csv, informe, excell, kml
    global dxf, municipio, provincia, dc, historicados
    answer_data = query.data
    posicional = answer_data
    for x in historicados:
        if x.parcela == answer_data:
            informe = x
            break
    if informe:
        keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
        data_btn = (('Archivo CSV', 'Archivo CSV'),
                    ('A DXF', 'A DXF'))
        data_btn1 = (('A KML', 'A KML'),
                     ('A Excell', 'A Excell'))

        provincia = informe.provincia
        municipio = informe.municipio
        dc = informe.dc
        parcela = informe.parcela
        area = informe.area
        texto = (
            f'El usuario {query.from_user.id}:{query.from_user.full_name} esta buscando {parcela} '
            f'del municipio {municipio}')
        aqui = Registrar(nombretelegram=query.from_user.full_name,
                         idtelegram=query.from_user.id,
                         consulta=texto)
        aqui.organiza()
        row_btn = (types.InlineKeyboardButton(text, callback_data=text)
                   for text, data in data_btn)
        keyboard_markup.row(*row_btn)
        row_btn1 = (types.InlineKeyboardButton(text, callback_data=text)
                    for text, data in data_btn1)
        keyboard_markup.row(*row_btn1)
        await query.message.reply(f"Parcela Historica {parcela}\nProvincia {provincia}\n"
                                  f"Municipio {municipio}\nDistrito catastral {dc}"
                                  f"\nArea {area} m2")
        csv = ("./archivos/csv/" + parcela + ".csv")
        excell = ("./archivos/excell/" + parcela + ".xlsx")
        kml = ("./archivos/kml/" + parcela + ".kml")
        dxf = ("./archivos/dxf/" + parcela + ".dxf")
        await query.message.answer("Que Deseas Hacer? ",
                                   reply_markup=keyboard_markup)


if __name__ == '__main__':
    mongo_setup()
    executor.start_polling(dp, skip_updates=True)
