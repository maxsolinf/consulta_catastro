import mongoengine


class ParcelaHistorica(mongoengine.Document):
    parcela= mongoengine.StringField()
    area = mongoengine.FloatField()
    provincia = mongoengine.StringField()
    municipio = mongoengine.StringField()
    dc = mongoengine.StringField()
    estado = mongoengine.StringField()
    georef = mongoengine.StringField()
    fuente = mongoengine.StringField()
    operador = mongoengine.StringField()
    coordenadas= mongoengine.MultiPointField()
    resultantes = mongoengine.ListField(mongoengine.ObjectIdField())
    consultas = mongoengine.IntField()
    interesados=mongoengine.ListField(mongoengine.ObjectIdField())

    meta = {
        'db_alias':'core',
        'collection':'historicas',
        'indexes':[
            'parcela',
            'provincia',
            'municipio',
            'dc'
           ]

    }

class ParcelaResultante(mongoengine.Document):
    posicional = mongoengine.IntField()
    operacion = mongoengine.StringField()
    comentario = mongoengine.StringField()
    expediente = mongoengine.IntField()
    parcelatemporal = mongoengine.StringField()
    area = mongoengine.FloatField()
    fechaCreaccion = mongoengine.DateField()
    fechaInscripcion = mongoengine.DateField()
    rowId = mongoengine.StringField()
    consultas= mongoengine.IntField()
    intereados = mongoengine.ListField(mongoengine.ObjectIdField())
    coordenadas= mongoengine.MultiPointField()
    meta={
        'db_alias':'core',
        'collection':'resultantes',
        'indexes': [
            'posicional',
            'expediente'
        ]
    }

class Usuarios(mongoengine.Document):
    nombre = mongoengine.StringField()
    apellido = mongoengine.StringField()
    cedula = mongoengine.StringField()
    telefono = mongoengine.IntField()
    nombretelegram = mongoengine.StringField()
    idtelegram = mongoengine.IntField()
    meta={
        'db_alias':'base',
        'collection':'usuarios',
        'indexes': [
            'cedula',
            'idtelegram',
        ]
    }

class Registro(mongoengine.EmbeddedDocument):
    fecha = mongoengine.DateTimeField()
    consulta = mongoengine.StringField()


class Visitas(mongoengine.Document):
    usuario = mongoengine.ObjectIdField()
    visitas = mongoengine.EmbeddedDocumentListField(Registro)
    meta = {
        'db_alias': 'base',
        'collection': 'visitas',
        'indexes': [
        'usuario',
        ]

    }



