from datetime import datetime
from nosql.clases import Registro, Usuarios, Visitas

class Registrar:
    def __init__(self,nombretelegram,idtelegram,consulta):
        self.idtelegram = idtelegram
        self.nombretelegram = nombretelegram
        self.consulta = consulta
        self.fecha = datetime.now()


    def reg_usuario(self):
        if Usuarios.objects(idtelegram=self.idtelegram):
            self.usuario=Usuarios.objects(idtelegram=self.idtelegram).first()
            return self.usuario
        else:
            self.usuario = Usuarios(nombretelegram=self.nombretelegram, idtelegram=self.idtelegram)
            self.usuario.save()
            return self.usuario

    def reg_visitante(self):
        if Visitas.objects(usuario=self.usuario.id):
            return Visitas.objects(usuario=self.usuario.id).first()
        else:
            self.visitante = Visitas(usuario=self.usuario.id)
            self.visitante.save()
            return self.visitante

    def reg_visita(self):
        self.registro = Registro(fecha=self.fecha,consulta=self.consulta)
        Visitas.objects(usuario=self.usuario.id).update_one(add_to_set__visitas=self.registro)

    def organiza(self):
        self.reg_usuario()
        self.reg_visitante()
        self.reg_visita()





