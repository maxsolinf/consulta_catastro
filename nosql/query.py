import logging
from nosql import mongo_set
from nosql.clases import ParcelaResultante, Usuarios, ParcelaHistorica
import requests
import random
import collections

logging.basicConfig(level=logging.INFO)


user_agent_list = ['Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
                   'Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36',
                   'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
                   'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
                   'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1']

user_agent = lambda: random.choice(user_agent_list)

def config_mongo():
    mongo_set.global_init()
def consulta(posicional):
    user = {'User-Agent': user_agent()} #cambio de navegador cada consulta de forma aleatoria
    """Funcion de consulta mediante la pagina de la jurisdiccion inmobiliaria nos devuelve un json con los parametros:
    (id), (x, y)Coordenadas geograficas,(utmx, utmy)Coordenadas planas, (provincia), (municipio), (area), 
    (coordenadas) en listas [x,y] planas, (status) de la peticion
"""
    
    data = {'posicional': f'{posicional}'}
    url_consulta = 'https://aplicaciones.ji.gob.do/LocalizadorDeInmuebles/GetResulting'
    r = requests.post(url_consulta, data=data, headers=user)
    logging.info(f"Buscando posicional {posicional} en la jurisdiccion inmobiliaria")
    respuesta = r.json()
    respuesta1 = collections.ChainMap({'Posicional': posicional}, respuesta)
    return collections.defaultdict(lambda: int(400), respuesta1)


def introduce_parcela(datos):
    resultado = consulta(datos)
    if resultado['status'] == 200:
        logging.info(f'Busqueda exitosa!!')
        parcela = ParcelaResultante()
        parcela.posicional = resultado['Posicional']
        parcela.area = resultado['area']
        parcela.coordenadas = resultado['coordinates']
        parcela.save()
        logging.info(f'Parcela {parcela.posicional} encontrada en la jurisdiccion inmobiliaria')
        logging.info(resultado)
        return parcela
    else:
        logging.info('Parcela {datos} No Encontrada')
        logging.info(resultado)
        return None



    
def busca_usuario(num):
    return Usuarios.objects(idtelegram=num).first

def universal_query(select: int, *, num=0, lat=0, long=0, cantidad=3):
    """ Con esta funcion tenemos como finalidad unir todos las querys de busqueda en una sola que
    sea sencilla y facil de depurar mediante un selector y un get de un diccionario con todas las
    querys ya introducidas ya en formato string obtenemos lo buscado por medio a un .get del dict"""
    dicter = {1: f'ParcelaResultante.objects(posicional={num}).first()',
              2: f'ParcelaResultante.objects(expediente={num}).first()',
              3: f'ParcelaResultante.objects(parcelatemporal={num}).first()',
              4: f'ParcelaResultante.objects(coordenadas__near={{"type":"Point","coordinates":[{long},{lat}]}},coordenadas__max_distance=150).limit({cantidad})',
              5: f'ParcelaHistorica.objects(coordenadas__near={{"type":"Point","coordinates":[{long},{lat}]}},coordenadas__max_distance=300).limit({cantidad})', }

    seleccion = dicter.get(select)
    if select > 0 and select <= 3:
        expediente = eval(seleccion)
        # logging.info(expediente)
        if not expediente:
            expediente = introduce_parcela(num)            
        return expediente
    elif select > 3 and select <= 5:
        expedientes = eval(seleccion)
        if expedientes:
            set_parcelas = set(expedientes)
            # for lugar in expedientes:
                # set_parcelas.add(lugar)
            return list(set_parcelas)
        else:
            return None


if __name__ == '__main__':
    config_mongo()
    resultante = 308325255175
    expediente = 321107053
    print(universal_query(1, num=resultante))
