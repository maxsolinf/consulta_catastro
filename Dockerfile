FROM python:3.8-alpine
# Instala Dependencias requeridas
RUN apk update --no-cache && apk add build-base git --no-cache
#Copia el contenido al directorio
ADD . /app
#hacemos el directorio /app
WORKDIR /app
#instala dependencias
RUN pip install -r requirements.txt
#abrimos los puertos de los Web hooks
EXPOSE 88
EXPOSE 80
EXPOSE 443
EXPOSE 8443
CMD ["python", "bot_main.py"]

