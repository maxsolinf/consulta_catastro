import datetime
import os
#os.chdir('/home/pi/consulta_catastro/')

def escribe_procesado(texto):
    variable = datetime.datetime.now().date()
    archivo_variable = f'LOG-{variable.day}-{variable.month}-{variable.year}.txt'
    try:
        with open(f'./logs/{archivo_variable}', 'a')as file:
            file.writelines(texto + '\n')
    except:
        with open(f'./logs/{archivo_variable}', 'w')as file:
            escribe_procesado(texto)
            pass
