import utm
from openpyxl import Workbook
import os
import ezdxf

# os.chdir=('/home/pi/consulta_catastro/')

wb = Workbook()
ws = wb.active


def crea_archivo(nombre, param: list):
    # with open(f'/home/pi/consulta_catastral/archivos/csv/{nombre}.csv', 'w') as file:
    try:
        with open('./archivos/csv/' + nombre + '.csv', 'r') as file:
            pass
    except FileNotFoundError:
        with open('./archivos/csv/' + nombre + '.csv', 'w') as file:
            p = 0
            for x in param:
                p += 1
                latlon = utm.from_latlon(x[1], x[0])
                file.write(f'{p},{latlon[0]},{latlon[1]},0,Punto\n')


def verifica_archivo(nombre, param: list):
    try:
        with open('./archivos/csv/' + nombre + '.csv', 'r') as file:
            # with open(f'/home/pi/consulta_catastral/archivos/csv/{nombre}.csv', 'r') as file:
            print('archivo existente')
            return True
    except IOError:
        print('creando archivo')
        crea_archivo(nombre, param)
        verifica_archivo(nombre, param)


def crea_xls(nombre, param: list):
    try:
        with open(f'./archivos/excell/{nombre}.xlsx', 'rb')as file:
            pass
    except FileNotFoundError:
        p = 0
        for x in param:
            p += 1
            latlon = utm.from_latlon(x[1], x[0])
            ws.append([p, latlon[0], latlon[1], 0, 'Punto'])
        wb.save(f'./archivos/excell/{nombre}.xlsx')
        # wb.save('/home/pi/consulta_catastral/archivos/excell/' + nombre + '.xlsx')


def crea_kml(nombre, param: list):
    try:
        with open(f'./archivos/kml/{nombre}.kml', 'r') as file:
            pass
    except FileNotFoundError:
        ecn1 = [
            '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n',
            '<Document>\n',
            f'<name> {nombre}.kml </name>\n',
            '<Style id = "poly">\n',
            '<LineStyle>\n',
            '<color> ff00ff55 </color>\n',
            '</LineStyle>\n',
            '<PolyStyle>\n',
            '<color> e8ff9257 </color>\n',
            '</PolyStyle>\n',
            '</Style>\n',
            '<Placemark>\n',
            '<styleUrl>  # poly</styleUrl>\n',
            '<Polygon>\n',
            '<outerBoundaryIs>\n',
            '<LinearRing>\n',
            '<coordinates>\n']
        ecn2 = ['</coordinates>\n',
                '</LinearRing>\n',
                '</outerBoundaryIs>\n',
                '</Polygon>\n',
                '</Placemark>\n',
                '</Document>\n',
                '</kml>\n']
        # with open('/home/pi/consulta_catastral/archivos/kml/' + nombre + '.kml', 'w') as file:
        with open(f'./archivos/kml/{nombre}.kml', 'w') as file:
            p = 0
            file.writelines(ecn1)
            for x in param:
                p += 1
                file.write(f'{x[0]},{x[1]},0 ')
            # file.write(f'{param[0][0]},{param[0][1]},0 ')
            file.write('\n')
            file.writelines(ecn2)


def crea_dxf(nombre, param: list):
    try:
        with open(f'./archivos/dxf/{nombre}.dxf', 'rb') as file:
            pass
    except FileNotFoundError:
        puntos = [utm.from_latlon(x[1], x[0])[:2] for x in param]
        # puntos.append(utm.from_latlon(param[0][1], param[0][0])[:2])
        doc = ezdxf.new(dxfversion='AC1032')
        doc.layers.new(f'{nombre}', dxfattribs={'color': 3, })
        msp = doc.modelspace()
        msp.add_lwpolyline(puntos, dxfattribs={'color': 7, 'layer': f'{nombre}_puntos'})
        # msp.add_text(f"Resutante {nombre}").set_pos(posi, align='MIDDLE_CENTER')
        # for x in puntos:
        #     msp.add_point(x.append(0), dxfattribs={'color':5})
        # msp.add_line(*param, dxfattribs={'color': 7})
        # doc.saveas(f'/home/pi/catastro_consulta/archivos/dxf/{nombre}.dxf')
        doc.saveas(f'./archivos/dxf/{nombre}.dxf')


if __name__ == '__main__':
    coords = [[
        -69.9357037082,
        18.4836480711
    ],
        [
            -69.935704751,
            18.4841019164
        ],
        [
            -69.9354737281,
            18.4840992619
        ],
        [
            -69.9354766147,
            18.4846662419
        ],
        [
            -69.9354747809,
            18.485410195
        ],
        [
            -69.9354773276,
            18.4869093654
        ],
        [
            -69.9363344116,
            18.4869106421
        ],
        [
            -69.9363336335,
            18.4862963866
        ],
        [
            -69.936976268,
            18.4862954678
        ],
        [
            -69.9369750715,
            18.4857442039
        ],
        [
            -69.937244817,
            18.4857437718
        ],
        [
            -69.9372449417,
            18.4854182503
        ],
        [
            -69.9371439767,
            18.4854182092
        ],
        [
            -69.9371441604,
            18.4850341271
        ],
        [
            -69.9372473061,
            18.4850341574
        ],
        [
            -69.9372478931,
            18.4847587921
        ],
        [
            -69.9370527805,
            18.4847587623
        ],
        [
            -69.9370528163,
            18.4840159041
        ]
    ]
    crea_dxf('macimo', coords)
